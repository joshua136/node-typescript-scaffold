# Project Name

Node/Typescript Boilerplate

# Description

This boilerplate helps with getting a node project started quickly with typescript. It tries to follow some of the nodejs best practises described [here](https://github.com/goldbergyoni/nodebestpractices) and more importantly follows the ['layer by components' structure](https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/projectstructre/breakintcomponents.md). It comes with modules for validation (built atop Joi), signing and verifying a jwt, logging (with mlite), error and exception handling as well as a db connection (mongodb) and a minature login system that can be built upon.

It also uses a linter and a formatter (eslint and prettier) to try to ensure code quality. The linter and formatter have been configured to run before any commit (a pre-commit hook powered by lint-staged and husky).

## Installation
This project uses Typescript as the development language. 

To get started:

- clone this repo
- run `npm install`. This will install all of the necessary dependencies and dev dependencies needed for the project
- run npx mrm@2 lint-staged to set up the precommit hook

Once installation is done, the dev server can be started with this command

`npm run serve`

For prod

`npm start`

# Project Structure

This boilerplate suggests the layer by compenents structure. See [here](https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/projectstructre/breakintcomponents.md) for why. Hence each major component of a project should be self contained in its own directory. By self contained, we mean that all of the routes, controllers, services, data access, tests etc for a component should be within its own directory. For example take a look at the `user` component in this boilerplate.

# Naming Convention

The following naming conventions are used throughout:

Variables - snake_case e.g `let user_name = "Jake"`

Global Constants / Static Variables - UPPER_SNAKE_CASE e.g `const BASE_ROUTE_MESSAGE = "message here"`

Enums - `enum name` - UpperCamelCase, `enum properties` - UPPER_SNAKE_CASE

```
enum TestEnum {
    PROPERTY_ONE = "prop 1"

    PROPERTY_TWO = "prop 2"
}
```

Classes - `class name` -  UpperCamelCase, `instance variables` - snake case, `static variables` - UPPER_SNAKE_CASE e.g

```
Class TestClass {
    instance_var: string
    static STATIC_VAR = true

    constructor(instance_var: string) {
        this.instance_var = instance_var
    }
}
```

Functions - lowerCamelCase e.g

```
function testFunction() {
    console.log("documentation");
}
```

# Error Handling

Errors are handled centrally in `error-handling/error-handler.ts` and all express errors are handled by the error handling middleware which in turn uses the functions in `error-handling/error-handler.ts`. 

It has some default Error types that extend from the centralized error object [AppException](https://friendly-wescoff-f7909c.netlify.app/classes/error_handling_base.appexception) and some that extend from the centralized error object [InAppException]()

- errors that extend from `InAppException` should be handled inside the app (it is best to throw these errors from services so they would be handled in the controller)
  
- errors that extend from  `AppException` or the `Error` object itself should be handled by the centralized error handler which would send back the appropriate response based on the error properties (it is best to throw these errors from controllers so they would be handled by the centralized error handler)

The advantage of handling errors by throwing exceptions where appropriate is that it reduces error handling logic in controllers and services.

## Errors that extend InAppException

- [InvalidPasswordException](./src/error-handling/invalid-password-exception.ts)

- [MissingArgumentException](./src/error-handling/missing-argument-exception.ts)

- [ObjectExistsException](./src/error-handling/object-exists-exception.ts)

- [ObjectNotFoundException](./src/error-handling/object-not-found-exception.ts)
  
- [PermissionException](./src/error-handling/permission-exception.ts)

## Errors that extend AppException

- [AlreadyExistsException](./src/error-handling/already-exists-exception.ts)
  
- [BadRequestException](./src/error-handling/bad-request-exception.ts)

- [ForbiddenException](./src/error-handling/forbidden-exception.ts)

- [GenericAppException](./src/error-handling/generic-app-exception.ts)

- [InternalServerException](./src/error-handling/internal-server-exception.ts)

- [NotFoundException](./src/error-handling/not-found-exception.ts)

- [UnauthorizedException](./src/error-handling/unauthorized-exception.ts)

- [ValidationException](./src/error-handling/validation-exception.ts)


# Validation

This boilerplate builds on top of `Joi` for data validation. It has handlers for most of the common Joi errors, and for unhandled erros, it provides a method to add new errors and error handlers.

## How to validate against a schema
Validating a data can be done like so:

```
Step 1 - Import validator class

import Validator from 'helpers/validator';

Step 2 - Create instance of validator

const validator = new Validator();

Step 3 - Call the validate method on the newly created instance

let result: ValidationStatus = validator.defineSchema(schema).validate(data);

Step 4 - Take action on the validation status

if (result.status === RuleStatus.INVALID) {
    // take action on failed validation
}

// take action of successful validation

```

The `validate` method returns an object of type ValidationStatus. See [here]((https://friendly-wescoff-f7909c.netlify.app/classes/helpers_validator_helper.validationstatus)) for more info on ValidationStatus and its properties.

## Adding a new Joi error type

In any case where any combination of Joi validation methods is used and results in an error which is not handled by default, the new error can be added in two ways. 

**NB: Joi Error types are usually of the form `blah.blah`. For example `string.empty`** See [here](https://joi.dev/api/?v=17.5.0#errors) for more.

### Approach 1 

With this approach, the error type would only be accessible on the validator instance on which it was added


Step 1 - Add the error type to the validator instance

```
validator.addErrorType("error.type")
```

Step 2 - In helpers/validator-helpers.ts, Add the error handler function for the new error type to the `RuleErrorTypes` object in UpperCamelCase format. For example error type string.empty will have error handler StringEmpty.

```
ErrorType: function(error: RuleError): InAppErrorResponse {
    return buildInAppError(ErrorNames.PAYLOAD_VALIDATION_ERROR, `${error.field} is required and must not be empty.`)
}
```

**NB: Each error handler function should return an object of type [InAppErrorResponse](https://friendly-wescoff-f7909c.netlify.app/modules/helpers_response.html#InAppErrorResponse)**

### Approach 2

Step 1 - Add the error type to the global default Joi errors list
```
In helpers/validator-helper.ts

Add the new error type to the `DEFAULT_JOI_ERRORS` array.
```

Step 2 - In helpers/validator-helpers.ts, Add the error handler function for the new error type to the `RuleErrorTypes` object in UpperCamelCase format. For example error type string.empty will have error handler StringEmpty.

```
ErrorType: function(error: RuleError): InAppErrorResponse {
    return buildInAppError(ErrorNames.PAYLOAD_VALIDATION_ERROR, `${error.field} is required and must not be empty.`)
}
```

# Linting

Linting on the project is powered by eslint and prettier is used to ensure proper code formatting.

# Example

See [user-auth](./src/components/user-auth) for an example on how to use the scaffold and some of modules it comes with.

# Contributing