import AppException, { ErrorData } from "./base";
import { HttpStatusCode } from "../helpers/httpStatusCodes";
import { ErrorNames } from "./common-errors";


class ServiceException extends AppException {
    constructor (data: ErrorData, isOperational = false) {
        const name = ErrorNames.DAL_EXCEPTION;
        const httpCode = HttpStatusCode.INTERNAL_SERVER_ERROR
        super(name, data, isOperational, httpCode);
    }
}

export default ServiceException