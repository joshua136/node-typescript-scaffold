import Validator from '../../helpers/validator';
import {
  buildInAppSucess,
  InAppErrorResponse,
  InAppResponse,
  InAppSuccessResponse,
  Status,
  SuccessResponse,
} from '../../helpers/response';
import { UserService } from './types';
import Joi from 'joi';
import { RuleStatus } from '../../helpers/validator-helper';
import ValidationException from '../../error-handling/validation-exception';
import {
  ALl_LOGOUT_SCHEMA,
  NEW_TOKEN_SCHEMA,
  SINGLE_LOGOUT_SCHEMA,
  USER_LOGIN_SCHEMA,
  USER_REG_SCHEMA,
} from './validation-schema';
import { User } from './models/user';
import GenericServerException from '../../error-handling/generic-app-exception';
import { CommonErrors, ErrorNames } from '../../error-handling/common-errors';
import { HttpStatusCode } from '../../helpers/httpStatusCodes';
import ObjectExistsException from '../../error-handling/object-exists-exception';
import AlreadyExistsException from '../../error-handling/already-exists-exception';
import ObjectNotFoundException from '../../error-handling/object-not-found-exception';
import InvalidPasswordException from '../../error-handling/invalid-password-exception';
import UnauthorizedException from '../../error-handling/unauthorized-exception';
import PermissionException from '../../error-handling/permission-exception';

export default class UserController {
  protected UserService: UserService;
  protected Validator: Validator;
  constructor(user_service: UserService, validator: Validator) {
    this.UserService = user_service;
    this.Validator = validator;
  }

  async register(user: object) {
    try {
      let result = this.validate(user, USER_REG_SCHEMA);

      result = await this.UserService.register(user as User);

      if (result.status === Status.ERROR)
        throw new GenericServerException({ message: CommonErrors.USER_REGISTRATION_ERROR });

      const success_response = result as InAppSuccessResponse;
      return new SuccessResponse(success_response.data, '', HttpStatusCode.CREATED);
    } catch (error) {
      if (error instanceof ObjectExistsException)
        throw new AlreadyExistsException({
          message: error.description,
          error_info: {
            error: error.description || ErrorNames.ALREADY_EXISTS,
            error_description: 'A user with these details exists already',
          },
          stack: error.stack,
        });

      throw error;
    }
  }

  async authenticate(data: { [key: string]: string }) {
    try {
      let result = this.validate(data, USER_LOGIN_SCHEMA);

      const user = data as unknown as User;
      result = await this.UserService.login(user, data['audience'], {
        ip_address: data['ip_address'],
        user_agent: data['user_agent'],
      });

      return new SuccessResponse(result.data, 'login successful', HttpStatusCode.OK);
    } catch (error) {
      if (error instanceof ObjectNotFoundException)
        throw new UnauthorizedException({ message: 'invalid email or phone number', stack: error.stack });

      if (error instanceof InvalidPasswordException)
        throw new UnauthorizedException({ message: error.description || 'invalid password', stack: error.stack });

      throw error;
    }
  }

  private validate(data: object, schema: Joi.Schema): InAppResponse {
    const result = this.Validator.defineSchema(schema).validate(data);

    if (result.status === RuleStatus.INVALID) {
      const error_details = result.details as InAppErrorResponse;
      throw new ValidationException({ message: error_details.message as string, context: data });
    }

    const success_details = result.details as InAppSuccessResponse;
    return buildInAppSucess(success_details.data);
  }

  async generateNewToken(data: { [key: string]: string }) {
    try {
      this.validate(data, NEW_TOKEN_SCHEMA);

      const { user_id, refresh_token, audience } = data;

      const result = await this.UserService.generateNewToken(user_id, refresh_token, audience);

      return new SuccessResponse(result.data || {}, 'access token generated succesfully', HttpStatusCode.OK);
    } catch (error) {
      if (error instanceof ObjectNotFoundException)
        throw new UnauthorizedException({ message: error.message || 'invalid user', stack: error.stack });

      if (error instanceof PermissionException)
        throw new UnauthorizedException({ message: error.description || '', stack: error.stack });
    }
  }

  async logout(data: { [key: string]: string }) {
    this.validate(data, SINGLE_LOGOUT_SCHEMA);

    const { user_id, refresh_token, access_token_id } = data;

    const result = await this.UserService.logout({ user_id, refresh_token, access_token_id });

    return new SuccessResponse(result.data || {}, 'logout succesful', HttpStatusCode.OK);
  }

  async logoutAll(data: { [key: string]: string }) {
    this.validate(data, ALl_LOGOUT_SCHEMA);

    const { user_id } = data;

    const result = await this.UserService.logoutAll(user_id);

    return new SuccessResponse(result.data || {}, 'logout succesful', HttpStatusCode.OK);
  }
}
