import { ObjectId } from 'mongodb';
import { UserDAL, UserService } from './types';
import { User } from './models/user';
import ObjectExistsException from '../../error-handling/object-exists-exception';
import { buildInAppError, buildInAppSucess, InAppResponse } from '../../helpers/response';
import { ErrorNames } from '../../error-handling/common-errors';
import ObjectNotFoundException from '../../error-handling/object-not-found-exception';
import bcrypt from 'bcrypt';
import InvalidPasswordException from '../../error-handling/invalid-password-exception';
import { generateToken } from '../../helpers/jwt';

const USER_ACCESS_TOKEN_EXPIRY = 900; // (in secs) token expires after 15 minutes

class UserAuthService implements UserService {
  protected UserDAL: UserDAL;

  constructor(dal: UserDAL) {
    this.UserDAL = dal;
  }

  async register(user: User) {
    const user_exists = await this.UserDAL.find(user.email || user.phone);

    if (user_exists) throw new ObjectExistsException('user exists already');

    let result: InAppResponse;

    const save_result = await this.UserDAL.save(user);

    if (save_result) {
      result = buildInAppSucess();
    } else {
      result = buildInAppError(ErrorNames.REGISTRATION_ERROR);
    }

    return result;
  }

  async login(user_data: User) {
    const user = await this.UserDAL.find(user_data.email || user_data.phone);
    if (!user) throw new ObjectNotFoundException('user does not exist');

    const result = await bcrypt.compare(user_data.password, user.password);
    if (!result) throw new InvalidPasswordException();

    const user_payload = { user_id: user._id, email: user_data.email || '', phone: user.phone || '' };
    const token_object = generateToken(user_payload, { is_app: false, expiresIn: USER_ACCESS_TOKEN_EXPIRY });

    return buildInAppSucess({ token: token_object.token });
  }

  public async getUser(key: string | ObjectId) {
    return await this.UserDAL.find(key);
  }
}

export default UserAuthService;
