import express, { NextFunction, Request, Response } from 'express';
import UserController from './controller';
import UserAuthService from './service';
import UserAuthDAL from './dal';
import Validator from '../../helpers/validator';
import { handleResponse } from '..';

const router = express.Router();

const service = new UserAuthService(new UserAuthDAL());
const validator = new Validator();

router.post(
  '/login',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const controller = new UserController(service, validator);
      return handleResponse(await controller.authenticate(req.body), res);
    } catch (error) {
      next(error);
    }
  },
);

router.post('/register', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const controller = new UserController(service, validator);
    return handleResponse(await controller.register(req.body), res);
  } catch (error) {
    next(error);
  }
});

export default router;
