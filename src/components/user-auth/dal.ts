import { ObjectId } from 'mongodb';
import UserModel from './models/user';
import { User } from './models/user';
import { UserDAL } from './types';

class UserAuthDAL implements UserDAL {

  async save(user: User) {
    const new_user = new UserModel(user);
    return await new_user.save();
  }

  async find(key: string | ObjectId) {
    if (typeof key !== 'string' && !ObjectId.isValid(key)) return {};

    if (ObjectId.isValid(key)) return await UserModel.findOne({ _id: new ObjectId(key) }).lean();
    else
      return await UserModel.findOne({
        $or: [{ email: key }, { phone: key }],
      }).lean();
  }
}

export default UserAuthDAL;
