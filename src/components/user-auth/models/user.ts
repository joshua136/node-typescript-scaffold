import mongoose, { Schema, Document } from 'mongoose';
import bcrypt from 'bcrypt';

export interface User extends Document {
  fullName: string;
  email: string;
  password: string;
  hash_password?: string;
  phone: string;
  dob: string;
}

const UserSchema: Schema<User> = new Schema(
  {
    full_name: {
      type: String,
      trim: true,
      required: true,
    },
    dob: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true,
      required: true,
    },
    phone: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    blacklisted: {
      type: Boolean,
      required: false,
      default: false,
    },
  },
  { timestamps: true },
);

// * Hash the password before it is saved to the database
UserSchema.pre('save', async function (next: (err: Error | null) => void) {
  // Make sure you don't hash the hash
  if (!this.isModified('password')) {
    return next(null);
  }
  this.password = await bcrypt.hash(this.password, parseInt(process.env.SALT_ROUNDS));
  next(null);
});

const UserModel = mongoose.model('User', UserSchema);

export default UserModel;
