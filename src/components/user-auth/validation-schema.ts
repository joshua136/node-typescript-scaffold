import Joi from 'joi';

export const USER_REG_SCHEMA = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().required(),
  full_name: Joi.string().required(),
  phone: Joi.string().required(),
  dob: Joi.string().required(),
});

export const USER_LOGIN_SCHEMA = Joi.object({
  email: Joi.string().email(),
  phone: Joi.string(),
  password: Joi.string().required(),
}).xor('email', 'phone');
