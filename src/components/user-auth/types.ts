import { ObjectId } from 'mongodb';
import { Document } from 'mongoose';
import { InAppResponse, InAppSuccessResponse } from '../../helpers/response';
import { User } from './models/user';

export interface UserService {
  register(user: User): Promise<InAppResponse>;

  /**
   *
   * @param user
   * @exceptions ObjectNotFoundException | InvalidPasswordException
   */
  login(user: User): Promise<InAppSuccessResponse>;

  /**
   * Retrives a user from the database
   * @param key
   */
  getUser(key: string | ObjectId): Promise<Document>;
}

export interface UserDAL {
  save(user: User): Promise<Document>;

  /**
   * Retrieves a user either by email, phone or user id
   * @param key
   */
  find(key: string | ObjectId): Promise<User>;
}

export type UserAccessTokenPayload = { user_id: ObjectId; email: string; phone: string; jti: string };

export type TokenFindOptions = {
  refresh_token?: string;
  user_id?: string;
  in_blacklist?: boolean;
  access_token_id?: string;
};

export interface RefreshToken {
  refresh_token: string;
  user_id: ObjectId;
  expiry: number;
  access_token_id: string;
}