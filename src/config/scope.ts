/**
 * A dictionary of scopes permmitted by the reverse proxy
 */
export const ALLOWED_SCOPES: { [key: string]: {[key: string]: string} } = {
    "bnpl": {
        prod: "bnpl.flutterwave.com",
        dev: "dev.bnpl.flutterwave.com"
    }
}

export type ResourceEnvs = { prod: string, dev: string }
export const AVAILABLE_ENVS = ["prod", "dev", "local"]