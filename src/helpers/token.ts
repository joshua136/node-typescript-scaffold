import crypto from "crypto";

const SECRET = '3220a85c8a1713b5';

/**
 * Generates a random unique string of 20 bytes. Useful for creating a public key
 * @returns string
 */
export function generatePublicID() {
    return crypto.randomBytes(20).toString('hex');
}

/**
 * Genrerates a cryptographically unique hmac digest from the key provided.
 * @param key a string (preferrably a unique one) to to use in generating the hmac string
 * @returns string
 */
export function generateSecretKey(key: string) {
    if (typeof key !== "string" || !key) throw new Error("key is required and must be of type string");
    return crypto.createHmac('sha256', SECRET).update(key).digest('hex');
}