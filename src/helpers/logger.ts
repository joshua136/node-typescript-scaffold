import mlite from "mlite";

/**
 * Describes methods required for any custom logger
 */
export interface MLogger {
    log(data: any, optional_key?: string, type?: string): any
    info(data: any, optional_key?: string): any
    warning(data: any, optional_key?: string): any
    error(data: any, optional_key?: string): any
    errorX(data: any, optional_key?: string): any
}

const Logger: MLogger = mlite(process.env.MLITEKEY || '61ae280d9a757b0004e0d6af');

export default Logger;