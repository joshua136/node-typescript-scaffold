import { ALLOWED_SCOPES } from "../config/scope";

/**
 * Checks if a variable holds a value that is an object
 * @param obj 
 * @returns boolean
 */
export function isObject(data: any) {
    return Object.prototype.toString.call(data) === '[object Object]';
}

/**
 * Removes properties that do not have valid values from an object
 * @param data object to be stripped of it's properties with invalid values
 * @returns an object with only properties that have valid values from the original object passed
 */
export function stripEmptyProperties(data: any) {
    if (!isObject(data)) return data;

    const new_data: {[key: string]: any} = {};
    for (const key in data) {
        if (data[key]) new_data[key] = data[key];
    }
    return new_data;
}

/**
 * Determines whether or not an object is empty
 * @param obj
 * @returns boolean
 */
export function objectIsEmpty(obj: any) {
    if (!isObject(obj)) return false;
    
    return Object.keys(obj).length === 0;
}

/**
 * Defines type for the payload signed using JWT (for application authorization)
 */
export type AccessTokenPayload = { app_id: string, registered_scopes: string }

export function flattenAllowedScopes(): string[] {
    const scopes = [];
    for (const scope in ALLOWED_SCOPES) {
       const urls = Object.values(scope);
       scopes.push(...urls);
    }
    return scopes;
}